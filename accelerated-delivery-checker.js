function main() {
//access budget performance report and select campaign name and delivery method
  var spreadsheet = SpreadsheetApp.openByUrl("insert spreadsheet here");
  var sheet = spreadsheet.getSheetByName("insert sheet name here or get active sheet");
  var report = AdWordsApp.report("SELECT BudgetName, DeliveryMethod FROM BUDGET_PERFORMANCE_REPORT");
  report.exportToSheet(sheet);

  //check for Standard delivery
  var lastRow = sheet.getLastRow();
  var campaigns = sheet.getRange(2, 1, lastRow, 2).getDisplayValues();

  for (i=0; i < lastRow; i++)

  {
  if (campaigns[i][1] === "Standard")
  {
    Logger.log(campaigns[i][0] + " is set to Standard Delivery, not Accelerated.");
    }

  else
  {
    Logger.log(campaigns[i][0] + " is set to Accelerated Delivery. Well done for adhering to best practice.");
  }
}
}
